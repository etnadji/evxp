"=== Initialisations ===========================================================

"--- Options ---------------------------------------------------------

" XSLT options
let g:exvp_xslt_proc = "xmlstarlet"
let g:exvp_xslt_open_output = "True"
let g:exvp_xslt_discover_style = "True"

" Validation options
let g:exvp_validation_discover = "True"
let g:exvp_validation_prefered = "rng"

" Saxon options
let g:exvp_saxon_ver = "8"
let g:exvp_saxon_6_bin_path = "/usr/share/java/saxon6/saxon.jar"
let g:exvp_saxon_8_bin_path = "/usr/share/java/saxon/saxon.jar"

let g:exvp_trang_bin_path = "/usr/share/java/trang.jar"

"---------------------------------------------------------------------

" Init python
python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))

" Create a global var for the plugin path
python plugin_path = vim.eval('expand("<sfile>:h")')
python vim.command('let g:Evxp_PluginPath = "{0}"'.format(plugin_path))

"=== Functions =================================================================

" Rules generation ---------------------------------------------------

function! BuildRNG()
python << endOfPython
from vim import *
from exvp import *
build_rules(vim,"rng")
endOfPython
endfunction

function! BuildRNC()
python << endOfPython
from vim import *
from exvp import *
build_rules(vim,"rnc")
endOfPython
endfunction

function! BuildXSD()
python << endOfPython
from vim import *
from exvp import *
build_rules(vim,"xsd")
endOfPython
endfunction

function! BuildDTD()
python << endOfPython
from vim import *
from exvp import *
build_rules(vim,"dtd")
endOfPython
endfunction

" XML validations ----------------------------------------------------

function! ValidateXML()
python << endOfPython
from vim import *
from exvp import *
validate_rules(vim,"formed")
endOfPython
endfunction

function! ValidateXSD()
python << endOfPython
from vim import *
from exvp import *
validate_rules(vim,"xsd")
endOfPython
endfunction

function! ValidateRNC()
python << endOfPython
from vim import *
from exvp import *
validate_rules(vim,"rnc")
endOfPython
endfunction

function! ValidateRNG()
python << endOfPython
from vim import *
from exvp import *
validate_rules(vim,"rng")
endOfPython
endfunction

function! ValidateDTD()
python << endOfPython
from vim import *
from exvp import *
validate_rules(vim,"dtd")
endOfPython
endfunction

function! Validate()
python << endOfPython
from vim import *
from exvp import *
validate_prefered_rules(vim)
endOfPython
endfunction

"---------------------------------------------------------------------

function! DoXSLT()
python << endOfPython
from vim import *
from exvp import *
do_xslt(vim)
endOfPython
endfunction

function! CountXPath()
python << endOfPython
from vim import *
from exvp import *
count = count_xpath(vim)
if count: print count
endOfPython
endfunction

"=== Commands ==================================================================

command! DoXSLT call DoXSLT()

command! CountXPath call CountXPath()

command! BuildRNG call BuildRNG()
command! BuildRNC call BuildRNC()
command! BuildXSD call BuildXSD()
command! BuildDTD call BuildDTD()

command! Validate call Validate()
command! ValidateXML call ValidateXML()
command! ValidateRNG call ValidateRNG()
command! ValidateRNC call ValidateRNC()
command! ValidateXSD call ValidateXSD()
command! ValidateDTD call ValidateDTD()
