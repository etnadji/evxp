#!/usr/bin/python2
# -*- coding:Utf-8 -*-

"""Python/Vim utilitary functions."""

# Imports ===============================================================#

import os

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#
# Fonctions =============================================================#

def edit_file(vim,filename):
    vim.command(
            ":e {0}".format(
                os.path.realpath(filename)
            )
    )

def setlocal(vim,options):
    if options:

        for option in options:

            if isinstance(option,dict):
                vim.command(
                        ":setl {0}={1}".format(
                            option["name"],
                            option["value"]
                        )
                )

            else:
                vim.command(
                        ":setl {0}".format(
                            option
                        )
                )

        return True

    else:
        return False

def vimvar(vim,variable):
    """Return the g:VARIABLE value."""
    return vim.eval("g:{0}".format(variable))

def refresh(vim,msg=False,msg_echom=False):
    """
    Refresh the Vim buffers and show a message.

    msg: False:  do nothing
         String: message to show

    msg_echom: use :echom to show a permanent message
    """

    if msg:
        if msg_echom:
            echom(vim,msg)
        else:
            print (msg)

    vim.command(":edit")

def normal(vim,keys,silent=True,end_in_normal_mode=False):
    """
    Do normal mode moves/commands.

    keys (str): sequence to execute in normal mode
    silent (bool): execute with silent! … or not
    end_in_normal_mode: Make Vim return into normal mode at the end of the sequence.
    """

    if end_in_normal_mode:
        if not keys.endswith("\<esc>"):
            keys = keys + "\<esc>"

    if silent:
        normal_maps = ':silent! execute "normal! {0}"'.format(keys)
    else:
        normal_maps = ':execute "normal! {0}"'.format(keys)

    vim.command(normal_maps)

def cat_in_buffer(vim,filepath,silent=True):
    """Cat/Dump a file into the current buffer"""

    if silent:
        vim.command(':silent! r !cat "{0}"'.format(filepath))
    else:
        vim.command(':r !cat "{0}"'.format(filepath))

def get_current_line(vim):
    """Shorcut function to get vim current buffer's current line"""

    try:
        return vim.current.buffer[vim.current.window.cursor[0]-1]
    except IndexError:
        return False

def get_next_line(vim):
    """
    Shorcut function to get current  vim buffer's line next to the
    current one.
    """

    try:
        return vim.current.buffer[vim.current.window.cursor[0]]
    except IndexError:
        return False

def get_user_input(vim,prompt,erase=True):
    """Use g:Gtg_Prompt as a variable to get user input"""

    vim.command('let g:exvp_prompt = input("{0} ")'.format(prompt))
    print (" ")

    uinput = to_unicode_or_bust(vim.eval("g:exvp_prompt"))

    if erase:
        vim.command('let g:exvp_prompt = "None"')

    return uinput

def echom(vim,message):
    """Shorcut function for :echom"""
    vim.command("""echom '{0}'""".format(message))

def to_unicode_or_bust(obj, encoding='utf-8'):
    """By Kumar McMillan"""

    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj = unicode(obj, encoding)
    return obj

# vim:set shiftwidth=4 softtabstop=4:
