#!/usr/bin/python2
# -*- coding:Utf-8 -*-

import os
import commands
import tempfile

from vim_utils import *

def validate_prefered_rules(vim):
    """Validate current XML file against prefered rules of prefered rule_type"""

    prefered = vimvar(vim,'exvp_validation_prefered')
    validate_rules(vim,prefered)

def validate_rules(vim,rule_type):
    """
    Validate current XML against rules of the current rule_type type.

    rule_type can be:
      - formed | Well formed XML (just checks structural validity)
      - dtd    | Document type definition
      - rng    | Relax-NG
      - rnc    | Relax-NG (compact)
      - xsd    | XML Schema Definition
    """

    if rule_type in ["dtd","rng","rnc","xsd","formed"]:

        if rule_type == "formed":
            rule_file = True

        else:
            # Get the rules

            rule_file = None

            discover = vimvar(vim,"exvp_validation_discover")
            if discover.lower() == "true": discover = True
            else: discover = False

            # Search for a rule file in the buffers...
            if discover:
                for buffer in vim.buffers:
                    bname = buffer.name.decode("utf8")
                    if bname.endswith(".{0}".format(rule_type)):
                        rule_file = bname; break

            # ... Or ask for a rule file path
            if rule_file is None:
                try:
                    rule_file = get_user_input(vim,"{0} file <".format(rule_type.upper()))
                except KeyboardInterrupt:
                    rule_file = False

        if rule_file:
            current_file = vim.current.buffer.name.decode("utf8")

            opts = {
                "rng":"--relaxng",
                "dtd":"--dtd",
                "xsd":"--xsd",
                "formed":"--well-formed"
            }

            if rule_type == "formed":
                command = 'xml val {0} {1}'.format(
                        opts[rule_type],
                        current_file
                    )
            else:
                command = 'xml val {0} {1} {2}'.format(
                        opts[rule_type],
                        rule_file,
                        current_file
                    )

            output = commands.getoutput(command)
            valid = False

            if output == '{0} - valid'.format(current_file):
                valid = True
            else:
                valid = False

                # Create the tempfile
                temp = tempfile.NamedTemporaryFile(
                    prefix="{0}_Validation_Error_".format(rule_type.upper()),
                    delete=False
                )

                # Header
                temp.write("Validate {0}\n".format(rule_type.upper()))
                temp.write("  File {0}\n".format(current_file))
                temp.write("  With   {0}\n".format(rule_file))
                temp.write("{0}\n".format(75*"="))

                # Validation output
                temp.write("Details\n")
                temp.write("{0}\n".format(75*"-"))
                temp.write(output)

                # Footer
                temp.write("\n{0}".format(75*"-"))

                # Close the file
                temp.close()

            vim.command(':redraw!')

            if rule_type == "formed":
                prefix = '{0} validation'.format(current_file)
            else:
                prefix = '{0} against {1} validation'.format(
                        current_file,
                        rule_type.upper()
                )

            if valid:
                echom(vim,'{0} [valid]'.format(prefix))
            else:
                vim.command(":sp {0}".format(temp.name))
                echom(vim,'{0} [errors]'.format(prefix))

def build_rules(vim,rule_type):
    """
    Generate rules for current XML file.

    rule_type can be:
      - dtd | Document type definition
      - rng | Relax-NG
      - rnc | Relax-NG (compact)
      - xsd | XML Schema Definition
    """
    if rule_type in ["dtd","rng","rnc","xsd"]:
        fout,fext = False,None

        if rule_type in ["dtd","rng","rnc","xsd"]:
            fout,fext = rule_type,rule_type
        else:
            echom(vim,"Invalid rules type.")

        if fout:
            if fext is not None:
                echom(vim,"Make {0} [running]".format(rule_type.upper()))

                in_file = vim.current.buffer.name.decode("utf8")
                out_file = "{0}.{1}".format(in_file.split(".")[0],fext)
                trang_path = vimvar(vim,"exvp_trang_bin_path")

                command = "java -jar {0} -I xml -O {1} {2} {3}".format(
                        trang_path,fout,in_file,out_file
                )

                output = commands.getoutput(command)

                if output:
                    # Create the tempfile
                    temp = tempfile.NamedTemporaryFile(
                        prefix="Build_{0}_Error_".format(rule_type.upper()),
                        delete=False
                    )

                    # Header
                    temp.write("Build {0}\n".format(rule_type.upper()))
                    temp.write("  From {0}\n".format(in_file))
                    temp.write("  To   {0}\n".format(out_file))
                    temp.write("{0}\n".format(75*"="))

                    if rule_type == "dtd":
                        if 'error: sorry, cannot handle this kind of "oneOrMore"' in output:
                            temp.write("=> The DTD file was not created.\n\n")

                    # Trang output
                    temp.write("Trang output\n")
                    temp.write("{0}\n".format(75*"-"))
                    temp.write(output)

                    # Footer
                    temp.write("\n{0}".format(75*"-"))

                    # Close the file
                    temp.close()

                vim.command(':redraw!')

                if output:
                    vim.command(":sp {0}".format(temp.name))
                    echom(vim,"Make {0} [errors]".format(rule_type.upper()))
                else:
                    echom(vim,"Make {0} [done]".format(rule_type.upper()))

def count_xpath(vim):
    """Count occurrences of XPath expression"""

    try:
        xpath_expr = get_user_input(vim,"XPath expr <")
    except KeyboardInterrupt:
        xpath_expr = False

    count = False

    if xpath_expr:
        buffer_name = vim.current.buffer.name.decode("utf8")

        count = commands.getoutput(
                'xml sel -t -v "count({0})" < {1}'.format(
                    xpath_expr,buffer_name
                )
        )

    return count

def do_xslt(vim):
    """Performs a XSLT transformation to the current buffer"""

    xml_in = False
    xslt_file = None
    current_file = vim.current.buffer.name.decode("utf8")

    # xml_in = xml source file
    # xml_out = xml output file (after xslt transformation)
    # xslt_file = xslt transformation file

    #--- Get source,xslt file,output file ----------------------------

    if current_file.endswith(".xml"):
        # Cas typique numéro 1
        # --------------------
        # le buffer XML est en cours d'édition donc le chemin du 
        # fichier de sortie est trouvé par Vim.

        xml_in = "%"
        xml_out = '%:r.html'

    else:
        # Le buffer en cours n'est pas un fichier XML

        if current_file.endswith(".xsl"):
            # Cas typique numéro 2
            # --------------------
            # Le buffer en cours est le fichier de transformation
            # On recherche parmi les buffers des sources XML
            # et on ajuste en conséquence

            bnames = [b.name.decode("utf8") for b in vim.buffers]
            bnames = [b for b in bnames if b.endswith(".xml")]

            xslt_file = '%'

            #echom(vim,"XSLT Transformation [canceled: must be run on a XML buffer]")

            if bnames:
                # Des sources XML dans les buffers

                if len(bnames) == 1:
                    # Il y a une seule source.
                    # On demande confirmation.

                    use = get_user_input(
                        vim,
                        "Use {0} as XML source [y/n] <".format(bnames[0])
                    )

                    if use.lower()[0] == "y":
                        xml_in = bnames[0]
                    else:
                        vim.command(':redraw!')

                else:
                    # Il y a plusieurs sources.
                    # On demande à l’utilisateur de choisir une d’entre elles.

                    cpt = 0
                    for buffname in bnames:
                        cpt += 1
                        print ("{0} - {1}".format(cpt,buffname))

                    try:
                        use = get_user_input(vim,"Select XML source [1-{0}] <".format(cpt))

                        try:
                            use = int(use)

                            if use <= len(bnames):
                                xml_in = bnames[use-1]
                            else:
                                echom(vim,"XSLT Transformation [Incorrect XML source choice]")
                        except ValueError:
                            echom(vim,"XSLT Transformation [Incorrect XML source choice]")

                    except KeyboardInterrupt:
                        pass

            else:
                # Pas de sources XML dans les buffers

                try:
                    xml_in = get_user_input(vim,"XML file <")
                except KeyboardInterrupt:
                    pass

        else:
            echom(vim,"XSLT Transformation [canceled: not a XML or XSL buffer]")

    if xml_in and xml_in != '%':
        xml_out = xml_in.split(os.sep)[-1]
        xml_out = ".".join(xml_out.split('.')[:-1])
        xml_out = xml_out + ".html"

    #-----------------------------------------------------------------

    if xml_in:
        # Get the XSLT processor
        xslt_proc = vimvar(vim,"exvp_xslt_proc")

        if xslt_file is None:
            # Get the XSL stylesheet
            discover = vimvar(vim,"exvp_xslt_discover_style")

            if discover.lower() == "true":
                discover = True
            else:
                discover = False

            # Search for a XSL file in the buffers...
            if discover:
                bnames = [b.name.decode("utf8") for b in vim.buffers]
                bnames = [b for b in bnames if b.endswith(".xsl")]

                if len(bnames) == 1:
                    xslt_file = bnames[0]
                else:
                    cpt = 0
                    for buffname in bnames:
                        cpt += 1
                        print ("{0} - {1}".format(cpt,buffname))

                    try:
                        use = get_user_input(vim,"Select XSL file [1-{0}] <".format(cpt))

                        try:
                            use = int(use)

                            if use <= len(bnames):
                                xslt_file = bnames[use-1]
                            else:
                                echom(vim,"XSLT Transformation [Incorrect XSL file choice]")
                        except ValueError:
                            echom(vim,"XSLT Transformation [Incorrect XSL file choice]")

                    except KeyboardInterrupt:
                        pass

            # ... Or ask for a XSL file path
            if xslt_file is None:
                try:
                    xslt_file = get_user_input(vim,"XSL file <")
                except KeyboardInterrupt:
                    xslt_file = False

        if xslt_file:
            # Make the transformation
            if xslt_proc in ["xmlstarlet","xalan","saxon"]:

                command = ''

                # Xmlstarlet
                if xslt_proc == "xmlstarlet":
                    command = "xml tr -o {0} {1} > {2}".format(xslt_file,xml_in,xml_out)

                # Xalan
                if xslt_proc == "xalan":
                    command = "Xalan -o {0} {1} {2}".format(xml_out,xml_in,xslt_file)

                # Saxon8 doesn't do XSLT 1.0 but XSLT 2.0+
                # Saxon6 doesn't do XSLT 2.0+ but 1.0+
                if xslt_proc == "saxon":
                    saxon_ver = vimvar(vim,"exvp_saxon_ver")

                    if not saxon_ver in ["6","8"]:
                        saxon_ver = "8"

                    saxon_bin = vimvar(vim,"exvp_saxon_{0}_bin_path".format(saxon_ver))

                    if saxon_ver == "8":
                        command = 'java -jar {0} -o:"{1}" {2} {3}'.format(
                            saxon_bin,xml_out,xml_in,xslt_file
                        )
                    if saxon_ver == "6":
                        command = "java -jar {0} -o {1} {2} {3}".format(
                            saxon_bin,xml_out,xml_in,xslt_file
                        )

                # XSLT transformation with xslt_proc

                if command:
                    print "in     |",xml_in
                    print "out    |",xml_out
                    print "xslt   |",xslt_file
                    print "command|",command
                    get_user_input(vim,"DEBUG")

                    echom(vim,"XSL transformation [running]")
                    vim.command(':silent !{0}'.format(command))
                    vim.command(':redraw!')
                    echom(vim,"XSL transformation [done]")
                    echom(vim,command)

                    # Open the XSLT output file if g:exvp_xslt_open_output = "True"

                    open_output = vimvar(vim,"exvp_xslt_open_output")
                    if open_output.lower() == "true":
                        open_output = True
                    else:
                        open_output = False

                    if open_output: vim.command(":e %:r.html")

            else:
                echom(vim,"XSL transformation: Unknown XSLT processor: '{0}'".format(xslt_proc))
