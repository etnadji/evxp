# EXVP - Etienne's XML Vim Plugin

**Github users : I’m leaving GitHub (because of Microsoft Acquisition).
Read [this](https://github.com/etnadji/301) for the new URL.
This repo will be ERASED soon.**

**Utilisateurs de Github : Je quitte GitHub (suite à son acquisition par Microsoft).
Lisez [ceci](https://github.com/etnadji/301) pour avoir la nouvelle URL.
Ce dépôt sera bientôt SUPPRIMÉ.**

Is a plugin to make Vim suit my needs in XML edition.

Designed to be used along [other XML configuration](http://etnadji.fr/blog/?d=2015/12/02/14/11/57-utiliser-vim-comme-editeur-xmlxslt),
it **does not aim** to be a complete XML plugin.

## Features

- XSLT transformation (by XSLT processors listed below).
- Generation of DTD/XSD/RNC/RNG (by *trang*)
- XML validation, well formed (by *Xmlstarlet*)
- XML validation, DTD/XSD/RNC/RNG (by *Xmlstarlet*)
- Count occurences of XPath expressions

## Installation

Use your plugin manager of choice.

- [vim-plug](https://github.com/junegunn/vim-plug)
  - Add `Plug 'https://github.com/etnadji/exvp'` to .vimrc
  - Run `:PlugInstall`
- [Vundle](https://github.com/gmarik/vundle)
  - Add `Bundle 'https://github.com/etnadji/exvp'` to .vimrc
  - Run `:BundleInstall`
- [NeoBundle](https://github.com/Shougo/neobundle.vim)
  - Add `NeoBundle 'https://github.com/etnadji/exvp'` to .vimrc
  - Run `:NeoBundleInstall`
- [Pathogen](https://github.com/tpope/vim-pathogen)
  - `git clone https://github.com/etnadji/exvp ~/.vim/bundle/exvp`

## Requirements

- Vim compiled with Python 2x support.
- [Xmlstarlet](http://xmlstar.sourceforge.net/)

### For XSLT transformation

One, or more of these softs:

- [Xmlstarlet](http://xmlstar.sourceforge.net/) (again)
- [Xalan](https://xml.apache.org/xalan-j/)
- [Saxon](http://saxon.sourceforge.net/)

### For DTD / XSD / RNC / RNG generation

- [Trang](http://www.thaiopensource.com/relaxng/trang.html).

### For DTD / XSD / RNC / RNG validation

- [Xmlstarlet](http://xmlstar.sourceforge.net/) (again++)
